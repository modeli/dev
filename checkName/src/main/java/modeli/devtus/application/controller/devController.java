package modeli.devtus.application.controller;

import modeli.devtus.application.dto.CalculEntree;
import modeli.devtus.application.dto.CalculSortie;
import modeli.devtus.application.service.CalculService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by M. KOUADGO on 22/02/2019.
 */
@Controller
public class devController {


    @Autowired
    private CalculService calculService;


    @RequestMapping(value = "/hello", method = RequestMethod.POST, consumes="application/json", produces = "application/json")
    public ResponseEntity<?> helloWorld(@RequestBody CalculEntree calculEntree) {
       return new
               ResponseEntity<CalculSortie> (calculService.determinerTauxdeVraisemblance(calculEntree), HttpStatus.OK);
    }
}
