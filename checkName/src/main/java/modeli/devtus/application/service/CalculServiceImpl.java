package modeli.devtus.application.service;

import modeli.devtus.application.dto.CalculEntree;
import modeli.devtus.application.dto.CalculSortie;
import org.springframework.stereotype.Service;

@Service
public class CalculServiceImpl implements CalculService {
    @Override
    public CalculSortie determinerTauxdeVraisemblance(CalculEntree c) {
        String resultat;
        double rate;
        if (c.getVariable1().equals(c.getVariable2())) {
            resultat ="OK";
            rate = 1;
        }
        else if (c.getVariable1().equalsIgnoreCase(c.getVariable2())) {
            resultat="OK";
            rate= new Double(0.5);
        }
        else if (c.getVariable1().trim().equalsIgnoreCase(c.getVariable2().trim())) {
            resultat="OK";
            rate= new Double(0.4);
        }
        else {
            resultat="KO";
            rate=0;
        }
        return new CalculSortie(rate,resultat);
    }
}
