package modeli.devtus.application.service;

import modeli.devtus.application.dto.CalculEntree;
import modeli.devtus.application.dto.CalculSortie;

public interface CalculService {
    public CalculSortie determinerTauxdeVraisemblance(CalculEntree c);
}