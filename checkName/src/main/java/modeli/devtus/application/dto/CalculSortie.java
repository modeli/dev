package modeli.devtus.application.dto;

public class CalculSortie {
    private double rate;
    private String resultat;

    public double getRate() {
        return rate;
    }

    public String getResultat() {
        return resultat;
    }

    public void setResultat(String resultat) {
        this.resultat = resultat;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public CalculSortie(double rate, String resultat) {
        this.rate = rate;
        this.resultat = resultat;
    }
}
