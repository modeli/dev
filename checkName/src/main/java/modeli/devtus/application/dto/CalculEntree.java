package modeli.devtus.application.dto;

public class CalculEntree {
    private String variable1;
    private String variable2;

    public String getVariable1() {
        return variable1;
    }

    public void setVariable1(String var1) {
        variable1 = var1;
    }

    public String getVariable2() {
        return variable2;
    }

    public void setVariable2(String var2) {
        variable2 = var2;
    }

    public CalculEntree(String var1, String var2) {
        variable1 = var1;
        variable2 = var2;
    }

    public CalculEntree() {

    }
}
